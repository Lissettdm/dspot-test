import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app.routing.module';
import { ComponentsModule } from './components/layout/layout.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { GatewayComponent } from './modules/gateway/gateway.component';
import { HttpConfigInterceptor } from './core/http/interceptos/http-config.interceptor';
import { CustomModule } from './components/customs/custom.module';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    ComponentsModule,
    CustomModule,
    RouterModule,
    AppRoutingModule,
  ],
  declarations: [
    AppComponent,
    GatewayComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
