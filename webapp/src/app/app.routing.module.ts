import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { GatewayModule } from './modules/gateway/gateway.module';
import { GatewayComponent } from './modules/gateway/gateway.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'gateway',
    pathMatch: 'full',
  },
  {
    path: 'gateway',
    component: GatewayComponent,
    children: [
      {
        path: '',
        loadChildren: './modules/gateway/gateway.module#GatewayModule'
      }]
  },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
