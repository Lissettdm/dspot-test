import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { IResponse } from '../models';


export default class Http {


  constructor(private _http: HttpClient) {
  }

  public get(endpoint: string, payload = {}): Observable<IResponse<any>> {
    return this._http.get(endpoint, { params: payload })
      .pipe(map(_m => <IResponse<any>>_m));
  }

  public post(endpoint: string, payload = {}): Observable<IResponse<any>> {
    return this._http.post(endpoint, payload, this.HandleRequestOptions())
      .pipe(map(_m => <IResponse<any>>_m));

  }

  public put(endpoint: string, payload = {}): Observable<IResponse<any>> {
    return this._http.put(endpoint, payload, this.HandleRequestOptions())
      .pipe(map(_m => <IResponse<any>>_m));
  }

  public delete(endpoint: string, payload = {}): Observable<IResponse<any>> {
    return this._http.delete(endpoint, this.HandleRequestOptions())
      .pipe(map(_m => <IResponse<any>>_m));

  }

  private HandleRequestOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json;charset=utf-8',
      })
    };
  }


}
