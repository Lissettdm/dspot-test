import { Injectable } from '@angular/core';
import { _ParseAST } from '@angular/compiler';
import { HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { MainStateService } from '../../state/main.state';
import { IResponse } from '../../models';


@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    count = 0;

    constructor(
        private mainState: MainStateService
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler) {
        setTimeout(() => {
            this.mainState.setNotification(null);
        }, 0);
        return next.handle(request).pipe(map(event => {
            if (event.type === HttpEventType.Sent && this.count === 0) {
                this.count++;
                this.mainState.setLoading(true);
            } else
                if (event instanceof HttpResponse) {
                    this.checkResponse(event);
                }
            return event;
        }), catchError(error => {
            this.mainState.setLoading(false);
            this.mainState.setNotification({
                type: 'error',
                message: 'UNHANDLE EXCEPTION - CONTACT THE SYSTEM ADMINISTRATOR',
            })
            return throwError(error);
        }));
    }

    checkResponse(event) {
        this.count--;
        const responseBody: IResponse<any> = event.body;
        if (!responseBody.success) {
            this.mainState.setNotification({
                type: 'error',
                message: responseBody.message + ' - ' + responseBody.data,
            })
        }
        if (this.count === 0) {
            this.mainState.setLoading(false);
        }
    }

}


