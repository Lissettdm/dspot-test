import { Injectable, Injector } from '@angular/core';
import { _ParseAST } from '@angular/compiler';
import { Observable } from 'rxjs';
import { ENDPOINTS } from './endpoints';
import { IGateway } from '../models/gateway/gateway.model';
import { IResponse, IGatewayList, IGatewayAddDevice } from '../models';
import Http from './http';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export default class GatewayService {
  private _http: Http

  constructor(
    private httpClient: HttpClient
  ) {
    this._http = new Http(this.httpClient);
  }

  public getAll(): Observable<IResponse<IGatewayList[]>> {
    return this._http.get(ENDPOINTS.gateway_getAll);
  }
  public get(serialNumber: string): Observable<IResponse<IGateway>> {
    return this._http.get(ENDPOINTS.gateway_get + serialNumber);
  }

  public add(payload: IGateway): Observable<IResponse<IGateway>> {
    return this._http.post(ENDPOINTS.gateway_add, payload);
  }

  public update(payload: IGateway): Observable<IResponse<IGateway>> {
    return this._http.post(ENDPOINTS.gateway_edit + payload.serialNumber, payload);
  }
  public adddevice(payload: IGatewayAddDevice): Observable<IResponse<IGateway>> {
    return this._http.post(ENDPOINTS.gateway_adddevice, payload);
  }

  public removedevice(gatewaySerialNumber: string, deviceUID: string): Observable<IResponse<IGateway>> {
    return this._http.delete(`${ENDPOINTS.gateway_removedevice}${gatewaySerialNumber}/${deviceUID}`);
  }

}

