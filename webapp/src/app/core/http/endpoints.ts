import { environment } from '../../../environments/environment';

export const ENDPOINTS = {
    gateway_getAll : `${environment.ApiURL}/gateway`,
    gateway_add : `${environment.ApiURL}/gateway`,
    gateway_get : `${environment.ApiURL}/gateway/`,
    gateway_edit : `${environment.ApiURL}/gateway/`,
    gateway_adddevice : `${environment.ApiURL}/gateway/device`,
    gateway_removedevice : `${environment.ApiURL}/gateway/`,

}