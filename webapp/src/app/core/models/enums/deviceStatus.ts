enum  DeviceStatus {
    Offline = 0,
    Online = 1
}

export default DeviceStatus;