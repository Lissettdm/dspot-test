import { IDevice } from '..';

export interface IGateway {
    serialNumber: string;
    humanReadable: string;
    IPv4Address: string;
    devices: IDevice[];
}
export default IGateway;
    
