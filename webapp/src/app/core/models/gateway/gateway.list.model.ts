
 interface IGatewayList {
    serialNumber: string;
    humanReadable: string;
    IPv4Address: string;
}

export default IGatewayList;
