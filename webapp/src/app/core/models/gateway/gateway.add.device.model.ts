import IDevice from '../device/device.model';

interface IGatewayAddDevice {
    serialNumber: string;
    device: IDevice;
}

export default IGatewayAddDevice