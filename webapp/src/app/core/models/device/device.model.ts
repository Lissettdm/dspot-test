import DeviceStatus from '../enums/deviceStatus';

interface IDevice {
    UID: number;
    vendor : string;
    dateCreated: Date;
    status: DeviceStatus;
}

export default IDevice;