interface IResponse<TEntity> {
    success: boolean;
    message: string;
    data: TEntity
}

export default IResponse;