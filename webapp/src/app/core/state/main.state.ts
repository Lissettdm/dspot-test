import { Injectable } from '@angular/core';
import { INotification, MainStateModel } from './models';


@Injectable({
    providedIn: 'root'
})
export class MainStateService {

    state : MainStateModel = null;

    constructor() {
        this.setInitialState();
    }

    setInitialState(): void {
        this.state = {
            title: '',
            subtitle: '',
            isLoading: false,
            notification: null,
        }
    }

    setNotification(data: INotification) {
        this.state.notification = data;
    }

    removeNotification() {
        this.state.notification = null;
    }

    setLoading(value) {
        this.state.isLoading = value;
    }

    setTitle(value) {
        this.state.title = value;
    }

    setSubtitle(value) {
        this.state.subtitle = value;
    }

}
