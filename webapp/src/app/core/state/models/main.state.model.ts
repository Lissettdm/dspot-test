import { INotification } from '.';

export default interface MainState {
    title: string;
    subtitle: string;
    isLoading: boolean;
    notification: INotification
}