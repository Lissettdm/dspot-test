export default interface INotification {
    type: string;
    message: string;
}