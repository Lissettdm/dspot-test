import { FormGroup } from '@angular/forms';
import VALIDATION_MESSAGES from '../resources/validation.messages';

export default class BaseForm {

    form: FormGroup;
    validations: any = {};


    OnChangeEvents(control: string) {
        this.SetValidationMessages(control);
    }

    SetValidationMessages(control: string) {      
        const formControl = this.form.get(control);
        if (formControl.dirty && formControl.invalid) {
            const error_keys = Object.keys(formControl.errors);
            const last_error_key = error_keys[error_keys.length - 1];
            this.validations[control] = VALIDATION_MESSAGES[last_error_key];
        } else {
            this.validations[control] = '';
        }
    }
}