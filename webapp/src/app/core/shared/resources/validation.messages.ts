const VALIDATION_MESSAGES = {
    required: 'Required Field',
    format: 'Invalid Format',
    ExistingUID: 'This UID value are taken',
  };

  export default VALIDATION_MESSAGES;