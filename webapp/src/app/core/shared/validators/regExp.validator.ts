import { ValidatorFn, AbstractControl, FormControl } from '@angular/forms';

const IPv4AddressValidator = (control: FormControl) => {
        const valid = isValidRegExp(control.value, /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/);
        return valid ? null : { 'format': { valid: false } };
}

const isValidRegExp = (value: any, regExp: RegExp): boolean => {
    const valid = regExp.test(value);
    return valid;
}

const Validators = {
    IPv4AddressValidator
}

export default Validators;