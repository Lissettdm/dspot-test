import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { ComponentsModule } from './components/layout/layout.module';
import { RouterTestingModule } from '@angular/router/testing';
import { NotificationComponent } from './components/customs/notification/notification.component';
import { HttpClientModule } from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule, ComponentsModule],
      declarations: [
        AppComponent,
        NotificationComponent,
      ],
    }).compileComponents();


  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });


});
