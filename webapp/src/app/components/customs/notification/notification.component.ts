import { Component, OnInit, Input } from '@angular/core';
import { INotification } from '../../../core/state/models';
import { MainStateService } from '../../../core/state/main.state';

@Component({
  selector: 'custom-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input() data: INotification = null;

  notificationTypes = {
    info: 'alert-info',
    warning: 'alert-warning',
    error: 'alert-danger'
  }

  constructor(
    private mainState: MainStateService
  ) { }

  ngOnInit() {
    this.mainState
  }

  close() {
    this.mainState.setNotification(null);
  }

}
