import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'custom-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  @Input() display = false;
  @Input() title = '';
  @Input() message = '';
  @Output() confirm: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {

  }

  onConfirm(): void {
    this.confirm.emit();
    this.display = false;
  }

  onCancel(): void {
    this.cancel.emit();
    this.display = false;
  }

}
