import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BtnBackComponent } from '../customs/buttons/btn-back/btn-back.component';
import { NotificationComponent } from './notification/notification.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { ErrorMessageComponent } from './input/messages/error-message/error-message.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    BtnBackComponent,
    NotificationComponent,
    ConfirmComponent,
    ErrorMessageComponent,
  ],
  exports: [
    BtnBackComponent,
    NotificationComponent,
    ConfirmComponent,
    ErrorMessageComponent


  ]
})
export class CustomModule { }
