import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'custom-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss']
})
export class ErrorMessageComponent implements OnInit {

  @Input() validation = null;
  constructor() { }

  ngOnInit() {
  }

}
