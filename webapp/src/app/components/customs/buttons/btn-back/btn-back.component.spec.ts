import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BtnBackComponent } from './btn-back.component';
import { Location } from '@angular/common';


describe('BtnBackComponent', () => {
  let component: BtnBackComponent;
  let fixture: ComponentFixture<BtnBackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [Location],
      declarations: [ BtnBackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
