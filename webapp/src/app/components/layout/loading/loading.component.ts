import { Component, OnInit } from '@angular/core';
import { MainStateService } from '../../../core/state/main.state';

@Component({
  selector: 'custom-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  constructor(
    public mainState: MainStateService
  ) { }

  ngOnInit() {
    
  }

}
