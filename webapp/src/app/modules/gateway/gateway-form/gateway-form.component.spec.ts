import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayFormComponent } from './gateway-form.component';
import { SHARED_MODULES } from '../../shared.module';
import { ActivatedRoute } from '@angular/router';

describe('GatewayFormComponent', () => {
  let component: GatewayFormComponent;
  let fixture: ComponentFixture<GatewayFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [...SHARED_MODULES],
      declarations: [ GatewayFormComponent ],
      providers: [ActivatedRoute]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
