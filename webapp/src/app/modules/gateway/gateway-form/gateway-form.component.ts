import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IGateway, IDevice, IGatewayAddDevice } from '../../../core/models';
import { GatewayService } from '../../../core/http';
import { Observable } from 'rxjs';
import { MainStateService } from '../../../core/state/main.state';
import { ValidatorRegExp } from '../../../core/shared/validators';
import { BaseForm } from '../../../core/shared/foms';
import { EnumHelper } from '../../../core/shared/helpers';
import DeviceStatus from '../../../core/models/enums/deviceStatus';

@Component({
  selector: 'app-gateway-form',
  templateUrl: './gateway-form.component.html',
  styleUrls: ['./gateway-form.component.scss']
})
export class GatewayFormComponent extends BaseForm implements OnInit {

  editmode = false;
  devices: IDevice[] = [];
  selectedDeviceUID = '';
  openDialog = false;

  @Output() onSave: EventEmitter<Observable<any>> = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private dataService: GatewayService,
    private mainState: MainStateService
  ) { 
    super();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) { /** edit option */
      this.editmode = true;
      this.mainState.setSubtitle('Edit');
      this.dataService.get(id)
        .subscribe(response => {
          if (response.success) {
            this.initForm(response.data);
          }
        });
    } else { /** add option */
      this.mainState.setSubtitle('Add');
      this.initForm(this.getEmptyModel());
    }
  }


  initForm(data: IGateway) {
    this.form = this.fb.group({
      serialNumber: [data.serialNumber, [Validators.required]],
      IPv4Address: [data.IPv4Address, [Validators.required, ValidatorRegExp.IPv4AddressValidator]],
      humanReadable: [data.humanReadable, [Validators.required]],
    });
    this.devices = data.devices;
  }
  cancel(): void {
    this.removeDeviceControl();
  }

  addDevice(): void {
    this.form.addControl('device', this.fb.group({
      UID: ['', [Validators.required]],
      vendor: ['', [Validators.required]],
      status: ['', [Validators.required]],
    }));
  }

  saveDevice(): void {
    const serialNumber = this.form.value.serialNumber;
    const device = <IDevice>this.form.value.device;
    device.dateCreated = new Date();
    if (this.editmode) {
      const model: IGatewayAddDevice = {
        serialNumber: serialNumber,
        device: device
      }
      this.dataService.adddevice(model)
        .subscribe(response => {
          if (response.success) {
            this.removeDeviceControl();
            this.devices = response.data.devices;
          }
        });
    } else {
      this.devices.push(device);
      this.removeDeviceControl();
    }

  }
  confirm(UID) {
    this.selectedDeviceUID = UID;
    this.openDialog = true;
  }

  removeDevice(): void {
    const serialNumber = this.form.value.serialNumber;
    const deviceUID = this.selectedDeviceUID;
    if (this.editmode) {
      this.dataService.removedevice(serialNumber, deviceUID)
        .subscribe(response => {
          if (response.success) {
            this.removeDeviceControl();
            this.devices = response.data.devices;
          }
        });
    } else {
      this.devices = this.devices.filter(_item => _item.UID.toString() !== deviceUID);
    }

  }

  removeDeviceControl() {
    this.form.removeControl('device');
  }

  getEmptyModel(): IGateway {
    return {
      serialNumber: '',
      IPv4Address: '',
      humanReadable: '',
      devices: []
    };
  }

  save(): void {
    let call = null;
    const model: IGateway = this.form.value;
    model.devices = this.devices;
    if (!this.editmode) {
      call = this.dataService.add(model);
      this.onSave.emit(call);
    }

  }
  getStatusName(value) {
    return EnumHelper.getEnumTypeName(value, DeviceStatus);
  }

  checkExistingUID(): void {
    const controlPath = 'device.UID'
    const UIDValue = this.form.get(controlPath).value;
    const found = this.devices.find(_item => _item.UID.toString() === UIDValue);
    if(found) {
      this.form.get(controlPath).setErrors({
        'ExistingUID': {
          valid: false,
        }
      });
      this.OnChangeEvents(controlPath);
    }
  }

}
