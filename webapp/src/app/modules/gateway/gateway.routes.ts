import { Routes } from '@angular/router';
import { GatewayListComponent } from './gateway-list/gateway-list.component';
import { GatewayEditComponent } from './gateway-edit/gateway-edit.component';


export const gatewayRoutes: Routes = [
    {
        path: '',
        component: GatewayListComponent,
    },
    {
        path: 'manage',
        component: GatewayEditComponent,
    },
    {
        path: 'manage/:id',
        component: GatewayEditComponent,
    },

];
