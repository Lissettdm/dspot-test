import { Component, OnInit } from '@angular/core';
import { GatewayService } from '../../../core/http';
import { IGatewayList, IResponse } from '../../../core/models';
import { Router } from '@angular/router';
import { MainStateService } from '../../../core/state/main.state';

@Component({
  selector: 'app-gateway-list',
  templateUrl: './gateway-list.component.html',
  styleUrls: ['./gateway-list.component.css']
})
export class GatewayListComponent implements OnInit {

  items: IGatewayList[] = [];


  constructor(
    private dataService: GatewayService,
    private router: Router,
    private mainState: MainStateService
  ) { }

  ngOnInit() {
    this.loadData();
    this.mainState.setSubtitle('Gateway Items');
  }

  loadData(): void {
    this.dataService.getAll()
    .subscribe((res: IResponse<IGatewayList[]>) => {
      this.items = res.data;
    });
  }

  add(): void {
    this.router.navigate(['gateway/manage']);
  }

  edit(serialNumber: string): void {
    this.router.navigate(['gateway/manage/'+ serialNumber]);

  }

}
