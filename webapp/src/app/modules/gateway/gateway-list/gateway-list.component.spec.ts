import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayListComponent } from './gateway-list.component';
import { GatewayService, HttpService } from '../../../core/http';
import { HttpClient } from '@angular/common/http';

describe('GatewayListComponent', () => {
  let component: GatewayListComponent;
  let fixture: ComponentFixture<GatewayListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GatewayListComponent ],
      providers: [HttpClient, HttpService, GatewayService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
