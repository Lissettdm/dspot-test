import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayEditComponent } from './gateway-edit.component';
import { GatewayModule } from '../gateway.module';

describe('GatewayEditComponent', () => {
  let component: GatewayEditComponent;
  let fixture: ComponentFixture<GatewayEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [GatewayModule],
      declarations: [ GatewayEditComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GatewayEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


});
