import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IGateway, IResponse } from '../../../core/models';
import { Location } from '@angular/common';

@Component({
  selector: 'app-gateway-edit',
  templateUrl: './gateway-edit.component.html',
  styleUrls: ['./gateway-edit.component.scss']
})
export class GatewayEditComponent implements OnInit {

  constructor(
    private location: Location,
  ) { }

  ngOnInit() {

  }

  save($event: Observable<any>) {
    $event.subscribe((response: IResponse<IGateway>) => {
      if(response.success) {
        this.location.back();
      }
    });
  }

}
