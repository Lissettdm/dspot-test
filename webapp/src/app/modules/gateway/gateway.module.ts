import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GatewayListComponent } from './gateway-list/gateway-list.component';
import { gatewayRoutes } from './gateway.routes';
import { GatewayFormComponent } from './gateway-form/gateway-form.component';
import { GatewayEditComponent } from './gateway-edit/gateway-edit.component';
import { SHARED_MODULES } from '../shared.module';


@NgModule({
  imports: [
    ...SHARED_MODULES,
    RouterModule.forChild(gatewayRoutes),
  ],
  declarations: [
    GatewayListComponent,
    GatewayFormComponent,
    GatewayEditComponent
  ],
  providers: [
  ]
})

export class GatewayModule {}
