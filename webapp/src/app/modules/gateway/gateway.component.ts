import { Component, OnInit } from '@angular/core';
import { MainStateService } from '../../core/state/main.state';
import { HttpClient } from '@angular/common/http';
import { GatewayService } from '../../core/http';

@Component({
  selector: 'app-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.css'],
})
export class GatewayComponent implements OnInit {

  constructor(
    private mainState: MainStateService,
    private hh: GatewayService,
  ) { 
    console.log(hh)
  }

  ngOnInit() {
    this.mainState.setTitle('Manage Gateway');
    
  }

}
