import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CustomModule } from '../components/customs/custom.module';

export const SHARED_MODULES = [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CustomModule,
]

