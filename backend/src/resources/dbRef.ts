const DBREF = {
    GATEWAY: {
        collection: 'gateway',
        options: { unique: 'serialNumber' }
    },
}


export default DBREF;