
import { DBService } from '../../services/DBService';
import { DBREF, TRANSSTRING } from '../../resources';
import * as express from 'express';
import BaseController from '../baseController';
import { Gateway, GatewayDevice } from '../../models';
import { GatewayState } from '../../models/states/gateway.state';
import { UTIL } from '../../helpers';
import util from '../../helpers/util';


class GatewayController extends BaseController {

    _db: DBService<Gateway> = new DBService<Gateway>(DBREF.GATEWAY.collection);

    constructor() {
        super();
    }


    getAll = async (req: express.Request, res: express.Response) => {
        try {
            // const data: Gateway = req.body;
            const dbData = await this._db.findAllBy();
            res.json(this.handleSuccess(dbData));


        } catch (error) {
            res.json(this.handleFail(error));
        }

    };

    getById = async (req: express.Request, res: express.Response) => {
        try {
            const id: number = req.params.id;
            const dbData = await this._db.find(id)
            res.json(this.handleSuccess(dbData));


        } catch (error) {
            res.json(this.handleFail(error));
        }

    };

    add = async (req: express.Request, res: express.Response) => {
        try {
            const data: Gateway = req.body;
            const gatewayState: GatewayState = new GatewayState(data);
            if(gatewayState.isValid()) {
                const state = gatewayState.getState();
                const dbData = await this._db.insert(state);
                res.json(this.handleSuccess(state));
            } else {
                res.json(this.handleModelFails(gatewayState.getErrorMessages()));
            }

        } catch (error) {
            res.json(this.handleFail(error));
        }

    };

    addDevice = async (req: express.Request, res: express.Response) => {
        try {
            const data: GatewayDevice = req.body;
            let gateway = UTIL.Clone(await this._db.find(data.serialNumber));
            if (gateway) {
                gateway.devices.push(data.device);
                const gatewayState = new GatewayState(gateway);
                if(gatewayState.isValid()) {
                   const state = gatewayState.getState(); 
                   const dbData = await this._db.update(state)
                    res.json(this.handleSuccess(state));
                } else {
                    res.json(this.handleModelFails(gatewayState.getErrorMessages()))
                }

            } else {
                throw new Error(`${TRANSSTRING.GATEWAY.SERIAL_NUMBER_NOT_FOUND}:  ${data.serialNumber}`)
            }
        } catch (error) {
            res.json(this.handleFail(error));
        }

    };

    remove = async (req: express.Request, res: express.Response) => {
        try {
            const id: number = req.params.id;
            const dbData = await this._db.delete(id)
            res.json(this.handleSuccess(dbData));

        } catch (error) {
            res.json(this.handleFail(error));
        }

    };

    removeDevice = async (req: express.Request, res: express.Response) => {
        try {
            const serialNumber = req.params.id;
            const devUID = req.params.UID;
            let gateway = await this._db.find(serialNumber);
            if (gateway) {
                gateway.devices = gateway.devices.filter(_dev => _dev.UID.toString() !== devUID);
                const dbData = await this._db.update(gateway)
                res.json(this.handleSuccess(dbData));
            } else {
                throw new Error(`${TRANSSTRING.GATEWAY.SERIAL_NUMBER_NOT_FOUND}:  ${serialNumber}`)
            }

        } catch (error) {
            res.json(this.handleFail(error));
        }

    };

    Copy(obj){
        return obj;
    }

}

export default new GatewayController();




