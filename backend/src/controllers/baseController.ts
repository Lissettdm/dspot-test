import { Response } from "../models";
import { TRANSSTRING } from "../resources";

export default class BaseController {

    handleSuccess = (responseObj: any) :Response => {
        return  {
            success: true,
            message: TRANSSTRING.MESSAGES.SUCCESS,
            data: responseObj
        };
    }

    handleFail = (responseObj: Error) :Response => {
        console.log(`${TRANSSTRING.MESSAGES.FAIL}: `, responseObj);
        return  {
            success: false,
            message: TRANSSTRING.MESSAGES.UNHANDLE,
            data: responseObj.message,
        };
    }

    handleModelFails = (responseObj: string[]) :Response => {
        console.log(`${TRANSSTRING.MESSAGES.FAIL}: `, responseObj);
        return  {
            success: false,
            message: TRANSSTRING.MESSAGES.FAIL,
            data: responseObj,
        };
    }

}