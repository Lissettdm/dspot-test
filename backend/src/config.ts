export const config = {
    dev: {
        PORT: '3000',
        origin: 'http://localhost:4200'
    },
    test: {
        PORT: '5000',
        origin: 'https://dspot-test-webapp.herokuapp.com'
    },
    production: {
        PORT: '3000',
        origin: 'https://dspot-test-webapp.firebaseapp.com'
    } 
};