import { TRANSSTRING } from "../resources";

const NotNullOrUndefined = (value) => {
    if (value === null || value === undefined) {
        return false;
    }
    return true;
}

const VALID_IPv4_FORMAT = (_val: string) => {
    const regex = /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/
    if (!regex.test(_val)) {
        return false
    }
    return true;
}


const VALID_MAX_LENGTH = (_val: any[], max: number = 0) => {
    if (NotNullOrUndefined(_val) && _val.length > max) {
        return false;
    }
    return true;
}

const NOT_DUPLICATED_VALUES = (_val: any[]) => {
    const values = new Set(_val);
    if(values.size !== _val.length) {
        return false;
    } 
    return true;
   
}

export default {
    VALID_MAX_LENGTH,
    VALID_IPv4_FORMAT,
    NOT_DUPLICATED_VALUES
}