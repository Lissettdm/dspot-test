class HttpModelStateError extends Error {
    constructor (errorMessages: string[]) {
        super();    
    }
}

class HttpUnhandleError extends Error {
    constructor (errorMessages: string) {
        super();    
    }
}

export default {
    HttpModelStateError,
    HttpUnhandleError
}