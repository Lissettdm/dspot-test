import * as express from 'express';
import GatewayRouter from './gateway';

export default class Route {

    routes(app: express.Application) {
        app.use('/gateway', GatewayRouter.routes());
    }


}