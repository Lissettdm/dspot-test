import * as express from 'express';
import { GatewayController } from '../controllers';

abstract class GatewayRouter {
    static router = express.Router();

    static routes() {
        /**gateways */
        this.router.get('/', GatewayController.getAll);
        this.router.get('/:id', GatewayController.getById);
        this.router.post('/', GatewayController.add);
        this.router.delete('/:id', GatewayController.remove);
        /**devices */
        this.router.post('/device', GatewayController.addDevice);
        this.router.delete('/:id/:UID', GatewayController.removeDevice);

        return this.router;
    }
}


export default GatewayRouter;