import express from 'express';
import bodyParser from "body-parser";
import { Route } from './routes';
import cors from 'cors';
import { config } from './config';

class App {
    app: express.Application = express();
    private route: Route = new Route();

    constructor() {
        const ENV = process.env.NODE_ENV || 'dev';
        const port = process.env.PORT || config[ENV].PORT;

        console.log(config[ENV].origin);
        
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));
        this.app.use(bodyParser.json());
        this.app.use(cors({origin: config[ENV].origin}));
        this.route.routes(this.app);
        this.app.listen(port, err => {
            if (err) {
                return console.error(err);
            }
            return console.log(`server is listening on ${port}`);
        });
    }
}

export default new App().app;






