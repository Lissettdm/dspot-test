import { Gateway, Device } from "..";
import BaseState from "./base/base.state";
import { VALIDATORS } from "../../helpers";
import { TRANSSTRING } from "../../resources";

export class GatewayState extends BaseState<Gateway> {

    constructor(model: Gateway) {
        super(model);
    }

    Validate() {
        if (!VALIDATORS.VALID_IPv4_FORMAT(this.getState().IPv4Address)) {
            this.addErrorMessages(`THE IPv4 ADDRESS ${TRANSSTRING.VALIDATORS.INVALID_IPv4_FORMAT}`);
        }
        if (!VALIDATORS.VALID_MAX_LENGTH(this.getState().devices, 10)) {
            this.addErrorMessages(`THE DEVICES ${TRANSSTRING.VALIDATORS.INVALID_MAX_LENGTH}`);
        }
        if (!VALIDATORS.NOT_DUPLICATED_VALUES(this.getState().devices.map(_m => _m.UID))) {
            this.addErrorMessages(`${TRANSSTRING.GATEWAY.DEVICE_SAME_UID}`);
        }

    }

    addDevice(device: Device) {
        if(device) {
            this._state.devices.push(device);
        }
    }

    isValid(): boolean {
        this.Validate();
        return super.isValid()
    }
}