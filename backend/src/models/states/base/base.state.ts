export default class BaseState<TEntity> {
    private _ErrorMessages: string[] = [];
    protected _state: TEntity = null;
    
    constructor(model: TEntity) {
        this._state = model;
    }

    protected isValid() {
        return this._ErrorMessages.length === 0;
    }

    addErrorMessages(value: string) {
        this._ErrorMessages.push(value);
    }

    getErrorMessages() {
        return this._ErrorMessages;
    }

    setState(value: TEntity) {
       this._state = value;
    }
    

    getState() {
        return this._state;
    }

}