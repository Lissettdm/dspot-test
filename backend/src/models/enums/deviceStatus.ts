enum  DeviceStatus {
    offline = 0,
    online = 1
}

export default DeviceStatus;