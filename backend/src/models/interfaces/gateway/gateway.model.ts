import Device from "../devices/device.model";

export default interface Gateway {
    serialNumber: string;
    humanReadable: string;
    IPv4Address: string;
    devices: Device[];
}

    