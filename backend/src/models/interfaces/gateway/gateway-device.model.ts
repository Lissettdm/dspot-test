import { Device } from "../..";

export default interface GatewayDevice {
    serialNumber: string;   
    device: Device;
} 