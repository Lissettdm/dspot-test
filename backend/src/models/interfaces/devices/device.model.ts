import { DeviceStatus } from "../../../models";

export default interface Device {
    UID: number;
    vendor : string;
    dateCreated: Date;
    status: DeviceStatus;
} 