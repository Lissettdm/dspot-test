export default interface Response {
    success: boolean;
    message: any;
    data: any
}