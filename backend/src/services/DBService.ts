
import MemoryDB from 'lokijs';
import { DBREF, TRANSSTRING } from '../resources';
import { UTIL } from '../helpers';
export class DBService<TEntity> {

    _MemoryDB: any = null;
    _repo: any;

    constructor(private collection: string) {
        this._MemoryDB = new MemoryDB('gateway_db.json', {
            autosave: false,
        });
        this.initDB();
    }

    initDB() {
        this.validateCollections();
    }

    insert = async (data: TEntity) => {
        this._repo.insert(data);
        return <TEntity>data;
    }

    update = async (data: TEntity) => {
        let entity = await this.find(this.getObjectByIdentifier(data));
        entity = data;
        this._repo.update(entity);
        return <TEntity>entity;
    }

    find = async (id: any) => {
        const entity = this._repo.findObject(this.getObjectByIdentifier(id));
        if (entity) {
            return <TEntity>entity;
        } else {
            throw new Error(`${TRANSSTRING.MESSAGES.NOTFOUND}`);
        }
    }

    findAllBy = async (data?: TEntity) => {
        let entities = null;
        if (data) {
            entities = this._repo.findObject(data);
        } else {
            entities = this._repo.data;
        }
        return <TEntity[]>entities;
    }

    delete = async (id: any) => {
        const entity = await this.find(this.getObjectByIdentifier(id));
        this._repo.remove(entity);
        return null;
    }

    getObjectByIdentifier(data) {
        const ref = Object.keys(DBREF).reduce((obj, key) => {
            if (DBREF[key].collection === this.collection) {
                const uniqueKey: string = DBREF[key].options.unique;
                const keyValue = data[uniqueKey] || data;
                obj[uniqueKey] = keyValue;
            }
            return obj;
        }, {});
        return ref;
    }

    validateCollections() {
        Object.keys(DBREF).forEach(key => {
            const collection = DBREF[key].collection;
            const options = DBREF[key].options;
            var existCollection = this._repo;
            if (!existCollection) {
                existCollection = this._MemoryDB.addCollection(collection, options);
            }
            this._repo = this._MemoryDB.getCollection(this.collection);

        });
    }
}