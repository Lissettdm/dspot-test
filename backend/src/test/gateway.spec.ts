process.env.NODE_ENV = 'test';

import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../index';
import { Gateway, Device, Response } from '../models';
import { TRANSSTRING, DBREF } from '../resources';
import { DBService } from '../services/DBService';
const should = chai.should();
chai.use(chaiHttp);


describe('Gateways', function () {

  it('should add a single Gateway POST', function (done) {
    chai.request(server)
      .post('/gateway')
      .send({
        "serialNumber": "454548",
        "humanReadable": "Gateway7",
        "IPv4Address": "192.168.3.240",
        "devices": [
          {
            "UID": 5874521,
            "vendor": "vendor",
            "dateCreated": new Date("2019-07-07"),
            "status": 1
          },
          {
            "UID": 5874522,
            "vendor": "vendor",
            "dateCreated": new Date("2019-07-07"),
            "status": 1
          },
          {
            "UID": 5874523,
            "vendor": "vendor",
            "dateCreated": new Date("2019-07-07"),
            "status": 1
          },
          {
            "UID": 5874524,
            "vendor": "vendor",
            "dateCreated": new Date("2019-07-07"),
            "status": 1
          },
          {
            "UID": 5874525,
            "vendor": "vendor",
            "dateCreated": new Date("2019-07-07"),
            "status": 1
          },
          {
            "UID": 5874526,
            "vendor": "vendor",
            "dateCreated": new Date("2019-07-07"),
            "status": 1
          },
          {
            "UID": 5874527,
            "vendor": "vendor",
            "dateCreated": new Date("2019-07-07"),
            "status": 1
          },
          {
            "UID": 5874528,
            "vendor": "vendor",
            "dateCreated": new Date("2019-07-07"),
            "status": 1
          },
          {
            "UID": 5874529,
            "vendor": "vendor",
            "dateCreated": new Date("2019-07-07"),
            "status": 1
          }
        ]
      })
      .end(function (err, res) {
        checkSuccessResponse(res);
        const data: Gateway = res.body.data;
        checkGatewayProps(data)
        const devices: Device[] = data.devices;
        devices.should.be.a('array');
        if (devices.length > 0) {
          const deviceElement = devices[0];
          checkDeviceProps(deviceElement);

        }
        done();
      });
  });


  it('should list all Gateways GET', function (done) {
    chai.request(server)
      .get('/gateway')
      .end(function (err, res) {
        checkSuccessResponse(res);
        const data: Gateway[] = res.body.data;
        data.should.be.a('array');
        if (data.length > 0) {
          const element = data[0];
          checkGatewayProps(element);
          const devices: Device[] = element.devices;
          devices.should.be.a('array');
          if (devices.length > 0) {
            const deviceElement = devices[0];
            checkDeviceProps(deviceElement);
          }
        }
        done();
      });
  });


  it('should get a Gateways by serial number GET', function (done) {
    chai.request(server)
      .get('/gateway/454548')
      .end(function (err, res) {
        checkSuccessResponse(res);
        const data: Gateway = res.body.data;
        checkGatewayProps(data);
        const devices: Device[] = data.devices;
        devices.should.be.a('array');
        if (devices.length > 0) {
          const deviceElement = devices[0];
          checkDeviceProps(deviceElement);
        }
        done();
      });
  });

  it('should add device to Gateways POST', function (done) {
    chai.request(server)
      .post('/gateway/device')
      .send({
        "serialNumber": "454548",
        "device": {
          "UID": 5874531,
          "vendor": "vendor",
          "dateCreated": "2019-07-07",
          "status": 1
        }
      })
      .end(function (err, res) {
        checkSuccessResponse(res);
        const data: Gateway = res.body.data;
        checkGatewayProps(data);
        const devices: Device[] = data.devices;
        devices.should.be.a('array');
        devices.length.should.be.above(0);
        const deviceElement = devices[0];
        checkDeviceProps(deviceElement);

        done();
      });
  });


  it('should remove device from Gateways DELETE', function (done) {
    let devicesCount = 0;
    chai.request(server)
      .get('/gateway/454548').end((err, res) => {
        const data: Gateway = res.body.data;
        devicesCount = data.devices.length;

        chai.request(server)
          .delete('/gateway/454548/5874531')
          .end(function (err, res) {
            checkSuccessResponse(res);
            const data: Gateway = res.body.data;
            data.devices.length.should.be.equal(devicesCount - 1);
            checkGatewayProps(data);
            const devices: Device[] = data.devices;
            devices.should.be.a('array');
            devices.length.should.be.above(0);
            if (devices.length > 0) {
              const deviceElement = devices[0];
              checkDeviceProps(deviceElement);
            }
            done();
          });

      });

  });



  it('should fails on invalid IPv4 Format in add Gateway POST', function (done) {
    chai.request(server)
      .post('/gateway')
      .send({
        "serialNumber": "454546",
        "humanReadable": "Gateway7",
        "IPv4Address": "192",
        "devices": []
      })
      .end(function (err, res) {
        checkFailModelResponse(res);
        const data = res.body.data;
        data.should.be.a('array');
        chai.expect(data).to.include(`THE IPv4 ADDRESS ${TRANSSTRING.VALIDATORS.INVALID_IPv4_FORMAT}`);
        done();
      });
  });

  it('should fails on existing device UID value on add Device to Gateway POST', function (done) {
    chai.request(server)
      .post('/gateway/device')
      .send({
        "serialNumber": "454548",
        "device": {
          "UID": 5874529,
          "vendor": "vendor",
          "dateCreated": "2019-07-07",
          "status": 1
        }
      })
      .end(function (err, res) {
        checkFailModelResponse(res);
        const data = res.body.data;
        data.should.be.a('array');
        chai.expect(data).to.include(`${TRANSSTRING.GATEWAY.DEVICE_SAME_UID}`);
        done();
      });
  });

  it('should fails on gateway Devices length > 10 in add Device to Gateway POST', function (done) {
    chai.request(server)
      .post('/gateway/device')
      .send({
        "serialNumber": "454548",
        "device": {
          "UID": 5874534,
          "vendor": "vendor",
          "dateCreated": "2019-07-07",
          "status": 1
        }
      })
      .end(function (err, res) {
        chai.request(server)
          .post('/gateway/device')
          .send({
            "serialNumber": "454548",
            "device": {
              "UID": 5874535,
              "vendor": "vendor",
              "dateCreated": "2019-07-07",
              "status": 1
            }
          })
          .end(function (err, res) {
            checkFailModelResponse(res);
            const data: Gateway = res.body.data;
            data.should.be.a('array');
            chai.expect(data).to.include(`THE DEVICES ${TRANSSTRING.VALIDATORS.INVALID_MAX_LENGTH}`);
            done();
          });
      });
  });

  it('should fails on add device with invalid serial number Gateway POST', function (done) {
    chai.request(server)
      .post('/gateway/device')
      .send({
        "serialNumber": "454545",
        "device": {
          "UID": 5874529,
          "vendor": "vendor",
          "dateCreated": "2019-07-07",
          "status": 1
        }
      })
      .end(function (err, res) {
        checkUnhandleFailResponse(res);
        const data = res.body.data;
        data.should.be.a('string');
        data.should.be.equal(`${TRANSSTRING.MESSAGES.NOTFOUND}`);
        done();
      });
  });

  it('should remove a Gateway DELETE', function (done) {
    chai.request(server)
      .delete('/gateway/454548')
      .end(function (err, res) {
        checkSuccessResponse(res);
        const data: Gateway = res.body.data;
        should.equal(data, null);
        done();
      });
  });

});

const checkSuccessResponse = (res) => {
  res.should.have.status(200);
  res.should.be.json;
  const body: Response = res.body;
  body.should.be.a('object');
  body.should.have.property('message');
  body.should.have.property('success');
  body.should.have.property('data');
  body.success.should.be.equal(true);
  body.message.should.be.equal(TRANSSTRING.MESSAGES.SUCCESS);
}

const checkFailModelResponse = (res) => {
  res.should.have.status(200);
  res.should.be.json;
  const body: Response = res.body;
  body.should.be.a('object');
  body.should.have.property('message');
  body.should.have.property('success');
  body.should.have.property('data');
  body.success.should.be.equal(false);
  body.message.should.be.equal(TRANSSTRING.MESSAGES.FAIL);
}

const checkUnhandleFailResponse = (res) => {
  res.should.have.status(200);
  res.should.be.json;
  const body: Response = res.body;
  body.should.be.a('object');
  body.should.have.property('message');
  body.should.have.property('success');
  body.should.have.property('data');
  body.success.should.be.equal(false);
  body.message.should.be.equal(TRANSSTRING.MESSAGES.UNHANDLE);
}

const checkGatewayProps = (element) => {
  element.should.have.property('serialNumber');
  element.should.have.property('humanReadable');
  element.should.have.property('IPv4Address');
  element.should.have.property('devices');
}

const checkDeviceProps = (deviceElement) => {
  deviceElement.should.have.property('UID');
  deviceElement.should.have.property('vendor');
  deviceElement.should.have.property('dateCreated');
  deviceElement.should.have.property('status');
}